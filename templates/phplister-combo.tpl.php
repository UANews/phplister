<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title><?php print $subject; ?></title>
</head>
<body leftmargin="0" marginwidth="0" topmargin="30" marginheight="0" offset="0" style="background-color: #e9e7d8;">
  <table border="0" cellpadding="0" cellspacing="0" width="100%" id="backgroundTable" bgcolor="#E9E7D8" style="padding-top: 50px;">
    <tr>
      <td valign="top">
        <table border="0" align="center" cellspacing="0" cellpadding="0" style="background-color: #FFFFFF; -moz-box-shadow: 0 0 6px 2px #CCC; -webkit-box-shadow: 0 0 6px 2px #CCC; box-shadow: 0 0 6px 2px #CCC; -moz-border-radius: 3px; border-radius: 3px;">
          <tr>
            <td bgcolor="#002147" style="height:32px; -webkit-box-sizing: content-box; -moz-box-sizing: content-box; box-sizing: content-box; padding: 5px 30px 5px 30px; box-shadow: 0 0 6px 2px #CCC; -moz-border-radius: 3px 3px 0 0; border-radius: 3px 3px 0 0 ;">
              <table border="0" align="left" cellspacing="0" cellpadding="0" >
                <tr>
                  <td>
                    <a href="http://arizona.edu"><span style="line-height: 0;"><img src="http://uaatwork.arizona.edu/sites/default/files/ua_a_line_logo_white.png" alt="The University of Arizona" /></span></a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>

          <tr>
            <td style="padding:0 30px 30px 30px;">
              <table border="0" width="627" align="center" cellspacing="0" cellpadding="0" style="background-color: #FFFFFF; margin:15px 0 0 0; padding:15px 0 10px 0; border-top: 1px dotted #CCCCCC; ">
                <tbody>
                  <tr>
                    <td rowspan="2">
                      <a href="http://uaatwork.arizona.edu/uaanounce"><img src="http://uaatwork.arizona.edu/sites/default/files/UAnnounceTitle.png" alt="UAnnounce: the online home for memos distributed by and to members of the University of Arizona community." /></a>
                    </td>
                    <td colspan="9" valign="middle" style="text-align: right; font-size: 14px; color: #003266;">
                      <?php print date('F j, Y'); ?>
                    </td>
                  </tr>
                  <tr>
                    <td width="30" valign="bottom" style="padding-bottom: 5px; text-align:right;">
                      <a href="http://www.facebook.com/uarizona" class="ss-facebook" target="_blank" title="Facebook">
                        <img src="http://uaatwork.arizona.edu/sites/all/themes/loquepasa-theme/social-pngs/facebook.png" alt="Facebook" />
                      </a>
                    </td>
                    <td width="30" valign="bottom" style="padding-bottom: 5px; text-align:right;">
                      <a href="http://twitter.com/uofa" class="ss-twitter" target="_blank" title="Twitter">
                        <img src="http://uaatwork.arizona.edu/sites/all/themes/loquepasa-theme/social-pngs/twitter.png" alt="Twitter" />
                      </a>
                    </td>
                    <td width="30" valign="bottom" style="padding-bottom: 5px; text-align:right;">
                      <a href="http://www.youtube.com/arizona" class="ss-youtube" target="_blank" title="YouTube">
                        <img src="http://uaatwork.arizona.edu/sites/all/themes/loquepasa-theme/social-pngs/youtube.png" alt="YouTube" />
                      </a>
                    </td>
                    <td width="30" valign="bottom" style="padding-bottom: 5px; text-align:right;">
                      <a href="http://www.linkedin.com/company/the-university-of-arizona" class="ss-tdnkedin" target="_blank" title="LinkedIN">
                        <img src="http://uaatwork.arizona.edu/sites/all/themes/loquepasa-theme/social-pngs/linkedin.png" alt="LinkedIN" />
                      </a>
                    </td>
                    <td width="30" valign="bottom" style="padding-bottom: 5px; text-align:right;">
                      <a href="http://instagram.com/uarizona " class="ss-instagram" target="_blank" title="Instagram">
                        <img src="http://uaatwork.arizona.edu/sites/all/themes/loquepasa-theme/social-pngs/instagram.png" alt="Instagram" />
                      </a>
                    </td>
                    <td width="30" valign="bottom" style="padding-bottom: 5px; text-align:right;">
                      <a href="http://pinterest.com/uofa " class="ss-pinterest" target="_blank" title="Pinterest">
                        <img src="http://uaatwork.arizona.edu/sites/all/themes/loquepasa-theme/social-pngs/pinterest.png" alt="Pinterest" />
                      </a>
                    </td>
                    <td width="30" valign="bottom" style="padding-bottom: 5px; text-align:right;">
                      <a href="http://www.arizona.edu/apps" class="ico-mobile" target="_blank" title="The University of Arizona - Mobile Apps">
                        <img src="http://uaatwork.arizona.edu/sites/all/themes/loquepasa-theme/social-pngs/mobile.png" alt="The University of Arizona - Mobile Apps" />
                      </a>
                    </td>
                    <td width="30" valign="bottom" style="padding-bottom: 5px; text-align:right;">
                      <a href="https://foursquare.com/uofa" class="ss-foursquare" target="_blank" title="FourSquare">
                        <img src="http://uaatwork.arizona.edu/sites/all/themes/loquepasa-theme/social-pngs/foursquare.png" alt="FourSquare" />
                      </a>
                    </td>
                    <td width="30" valign="bottom" style="padding-bottom: 5px; text-align:right;">
                      <a href="http://uaatwork.arizona.edu/uaatwork.xml" class="ss-rss" title="RSS">
                        <img src="http://uaatwork.arizona.edu/sites/all/themes/loquepasa-theme/social-pngs/rss.png" alt="RSS" />
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
              <table border="0" width="627" align="center" cellspacing="0" cellpadding="0" style="margin: 15px 0 15px 0; padding:15px 0 0 0; border-top:dotted 1px #CCC;">
                <tr>
                  <td>
                    <span class="atwork-home" style="margin-right:22px;"><a href="http://uaatwork.arizona.edu"><img src="http://uaatwork.arizona.edu/sites/default/files/atWorkButton.png" alt="UA at Work Homepage" /></a></span>
                    <span class="uannounce-link"><a href="http://uaatwork.arizona.edu/uannounce"><img src="http://uaatwork.arizona.edu/sites/default/files/UAnnounceButton.png" alt="UAnnounce page on UA at Work" /></a></span>
                  </td>
                  <td align="right">
                    <span class="manage-subscription" style=";"><a href="<?php print $subscribe_url; ?>"><img src="http://uaatwork.arizona.edu/sites/default/files/SubscriptionButton.png" alt="Manage your subscription" /></a></span>
                  </td>
                </tr>
              </table>
              <table>
                <tbody>
                  <tr>
                    <td>
                      <table border="0" width="627" align="center" cellspacing="0" cellpadding="0" >
                        <tr>
                          <td>
                              <?php print $allua; ?>
                              <?php print $campus; ?>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#E9E7D8" style="padding-top:0 ;">
    <tr>
      <td>
        <table width="627" align="center" style="margin-top:15px; background-color: #E9E7D8">
          <tr>
            <td style="text-align: center; width: 100%; padding-top: 10px; padding-bottom: 10px; border-top: 1px dotted #ccc; border-bottom: 1px dotted #ccc;">
              <a href="http://uaatwork.arizona.edu/about-uannounce" style="display: inline-block; color: #036; text-decoration:none;">About UAnnounce</a> | <a href="http://uaatwork.arizona.edu/about-classifieds" style="display:inline-block; color: #036; text-decoration:none;">How to Buy an Ad</a> | <a href="<?php //print $subscribe_url; ?>" style="display:inline-block; color: #037; text-decoration:none;">Subscribe</a> | <a href="http://www.uanews.org" style="display:inline-block; color: #036; text-decoration:none;">UANews.org</a>
            </td>
          </tr>
        </table>
        <table width="627" align="center" style="text-align: center; padding-top: 10px; padding-bottom:40px; background-color: #E9E7D8">
          <tr>
            <td style="padding:0">
              <p style="margin:0; padding:0; font-size: 12px; font-weight:bold; text-align:center; ">UAnnounce is produced by the Office of University Communications</p>
            </td>
          </tr>
          <tr>
            <td style="padding:0">
              <p class="address" style="margin:0; padding:0; font-size: 12px; text-align:center;">888 N. Euclid Ave., Ste. 413 <span style="font-style:italic;">(or)</span> P.O. Box 210158, Tucson, AZ 85721 | <span class="telephone"><span style="color:#C72334;">T</span> 520.621.1877 </span><span class="fax"><span style="color:#C72334;">F</span> | 520.626.4121</span></p>
            </td>
          </tr>
          <tr>
            <td style="padding:0">
              <p style="margin:0; color:#666; font-size:11px; text-align:center">To manage your subscription use <a href="<?php print $unsubscribe_url; ?>" style="color:#C52433;">this link</a>.</p>
            </td>
          </tr>
          <tr>
            <td style="padding:0">
              <p style="margin:0; font-size:11px; color:#666; text-align:center; ">&copy; 2013 Arizona Board of Regents</p>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  <?php //print $open_tracking; ?>
  </table>
</body>
</html>
