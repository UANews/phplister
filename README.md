All-UA: Immediate Delivery
    On All-UA node_save() pipe message to phpList
    Mark node as sent in `phplister_sent_items` as immediate


All-UA: Daily Digest
    At 1 O'Clock gather messages of the day
    Select all node UAnnounce of type All-UA from where the publish date is
    > (now - 24hrs) cross NOT IN `phplister_sent_items` where
    field_sent_item_value is Daily

All-UA: Weekly Digest
    At an hour of a day of the week gather all messages that have been that have
    not been sent

Campus Notes: Immediate Delivery

Campus Notes: Daily Digest

Campus Notes: Weekly Digest

Combined Daily: All-UA + Campus Notes

Combined Weekly: All-UA + Campus Notes

Need a marking system for having been sent immediately, daily, and weekly.
Mark as "Sent to PHPList" rather than just sent.

Immediates are handles on a hook_node_save() while Daily and Weekly are handled
by Drupal Cron
